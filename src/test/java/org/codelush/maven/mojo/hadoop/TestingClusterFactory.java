package org.codelush.maven.mojo.hadoop;

/*
 * Copyright 2014 CodeLush.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hdfs.MiniDFSCluster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Daniel on 1/25/14.
 */
public class TestingClusterFactory
{

	private static final Logger Log = LoggerFactory.getLogger(TestingClusterFactory.class);

	public static MiniDFSCluster buildAndStartMiniDfsCluster(Configuration conf)
	{
		MiniDFSCluster cluster = null;
		try
		{
			File currentDir = new File("");
			if ((new File("")).getAbsolutePath().endsWith("target/"))
			{
				conf.set(MiniDFSCluster.HDFS_MINIDFS_BASEDIR, "target/hadoop/hdfs/");
			} else
			{
				conf.set(MiniDFSCluster.HDFS_MINIDFS_BASEDIR, "hadoop/hdfs/");
			}
			logClusterConfiguration(conf);

			MiniDFSCluster.Builder builder = new MiniDFSCluster.Builder(conf);
			builder.numDataNodes(3);
			builder.format(true);
			cluster = builder.build();
			cluster.waitActive();
		} catch (IOException e)
		{
			Log.error("Could not create and or start the cluster", e);
		} finally
		{
		}
		return cluster;
	}

	private static void logClusterConfiguration(Configuration conf)
	{
		Iterator<Map.Entry<String, String>> itr = conf.iterator();
		StringBuilder msgBuilder = new StringBuilder();
		while (itr.hasNext())
		{
			Map.Entry<String, String> valuePair = itr.next();
			msgBuilder.append(valuePair.getKey() + "=" + valuePair.getValue() + "\n");
		}
		Log.info("Building Cluster with configuration:\n{}", msgBuilder);
	}
}
