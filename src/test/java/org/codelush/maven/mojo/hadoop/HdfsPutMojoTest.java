package org.codelush.maven.mojo.hadoop;

/*
 * Copyright 2014 CodeLush.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.permission.FsAction;
import org.apache.hadoop.fs.permission.FsPermission;
import org.apache.hadoop.hdfs.MiniDFSCluster;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.security.UserGroupInformation;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.security.PrivilegedExceptionAction;

/**
 * Created by Daniel on 1/22/14.
 */
public class HdfsPutMojoTest extends AbstractMojoTestCase
{
	private static final Logger Log = LoggerFactory.getLogger(HdfsPutMojoTest.class);

	private static MiniDFSCluster cluster;
	private static JobConf conf;
	private static FileSystem fs;

	private static final String HADOOP_TEST_USER = "hadoop-test-user";

	protected void setUp() throws Exception
	{
		// Required for Mojo Lookups
		super.setUp();
		setupCluster();
	}

	private static void setupCluster() throws IOException
	{
		try
		{
			conf = new JobConf();
			setupToAllowProxyUser(conf);
			cluster = TestingClusterFactory.buildAndStartMiniDfsCluster(conf);

			fs = FileSystem.get(conf);

			// Setup Cluster Root to be writable to all to facilitate proxying users
			fs.setPermission(new Path("/"), new FsPermission(FsAction.ALL, FsAction.ALL, FsAction.ALL));
		} catch (IOException ex)
		{
			Log.error("Could not start up Hadoop Mini HDFS Cluster", ex);
			fail("Could not start up Hadoop Mini HDFS Cluster: " + ex.getLocalizedMessage());
		}
	}

	private static void setupToAllowProxyUser(Configuration conf) throws IOException
	{
		String loginUser = UserGroupInformation.getLoginUser().getUserName();
		conf.set("hadoop.proxyuser." + loginUser + ".groups", "*");
		conf.set("hadoop.proxyuser." + loginUser + ".hosts", "*");
	}

	@AfterClass
	public static void cleanUpTests()
	{
		cluster.shutdown();
	}

	public void testMojoLookup()
	{
		HdfsPutMojo mojo = getHdfsPutMojo("src/test/resources/unit/plugin-config.xml");
		assertNotNull(mojo);
	}

	public void testMojoExecute()
	{
		HdfsPutMojo mojo = getHdfsPutMojo("src/test/resources/unit/plugin-config.xml");
		assertNotNull(mojo);
		try
		{
			mojo.execute();
		} catch (MojoExecutionException e)
		{
			Log.error("Could not execute Mojo", e);
			fail("Could not execute Mojo because " + e.getLocalizedMessage());
		}
	}

	private HdfsPutMojo getHdfsPutMojo(String pomXml)
	{
		File pluginXml = new File(getBasedir(), pomXml);
		HdfsPutMojo mojo = null;
		try
		{
			mojo = (HdfsPutMojo) lookupMojo("put", pluginXml);
		} catch (Exception e)
		{
			Log.error("Could not get {} Mojo for {}", HdfsPutMojo.class.getSimpleName(), pluginXml);
		}
		return mojo;
	}

	public void IgntestWrite() throws IOException, InterruptedException
	{
		UserGroupInformation ugi =
				UserGroupInformation.createProxyUser(HADOOP_TEST_USER,
				                                     UserGroupInformation.getLoginUser());
		ugi.doAs(new PrivilegedExceptionAction<Void>()
		{

			public Void run() throws Exception
			{

				fs.mkdirs(new Path("/test"));
				return null;
			}
		});
	}
}
