package org.codelush.maven.mojo.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.util.List;

/**
 * Created by Daniel on 1/25/14.
 */
public class HdfsUtil
{
	public static void copyFromLocal(Configuration conf, String destination, List<Path> srcPaths) throws IOException
	{
		Path dstPath = new Path(destination);
		FileSystem dstFs = dstPath.getFileSystem(conf);
		dstFs.copyFromLocalFile(false, false, srcPaths.toArray(new Path[srcPaths.size()]), dstPath);
	}
}
