package org.codelush.maven.mojo.hadoop;

/*
 * Copyright 2014 CodeLush.org
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Goal which
 */
@Mojo(name = "put", defaultPhase = LifecyclePhase.INSTALL, threadSafe = true)
public class HdfsPutMojo extends AbstractMojo
{
	/**
	 * Location of the file.
	 */
	@Parameter(property = "localSource", required = true)
	private String localSource;

	@Parameter(property = "localSources", required = true)
	private String[] localSources;

	@Parameter(property = "destination", required = true)
	private String destination;

	public void execute() throws MojoExecutionException
	{
		logProperties();
		List<Path> sources = getSources();

		if (sources.isEmpty())
		{
			throw new MojoExecutionException("No Sources where provided");
		}

		try
		{
			HdfsUtil.copyFromLocal(new Configuration(), destination, sources);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private List<Path> getSources()
	{
		List<Path> sources = new ArrayList<Path>();
		if (localSource != null && !localSource.isEmpty())
		{
			sources.add(new Path(localSource));
		}

		if (localSources != null)
		{
			for (String path : localSources)
			{
				if (path != null && !path.isEmpty())
				{
					sources.add(new Path(path));
				}
			}
		}
		return sources;
	}

	private void logProperties()
	{
		if (getLog().isInfoEnabled())
		{
			getLog().info("Local Source: " + localSource);
			getLog().info("Local Sources: " + localSources);
			getLog().info("Destination: " + destination);
		}
	}
}
